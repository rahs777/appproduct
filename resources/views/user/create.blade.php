@extends('layouts.admin360')

@section('content')
 <div class="row">

	 	
	         @include('custom.message')

	<div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/person-plus.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Ingrese Usuario </span>
             </strong>
         </div class="panel-heading clearfix">
        <fieldset >
        	  <form method="POST" action="{{ route('user.store') }}" >
						
									 @csrf
									 <div></div>
								<div class="card1">
							<!--================[]==================-->
							      
										<div class="col-sm-5">
											<div class="form-group label-floating" >
												<label class="control-label" ><i class="fa fa-user"> </i>   {{ __('Nombre') }} </label>
				                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

				                                @error('name')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                          </div>
				                         
				                          </div>
				                           <!--================[]==================-->
				                          {{csrf_field()}}

									<input type="hidden" name="status" value="0">
										<div class="col-sm-5">
					                        <div class="form-group label-floating" >
					                            <label class="control-label" for="username" ><i class="fa fa-user"> </i>  {{ __('Nombre de Usuario') }}</label>
					                      
					                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

					                                @error('name')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
                       
										<!--================[]==================-->
									<div class="col-sm-5">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-envelope-o"> </i>  {{ __('Correo') }}</label>
		                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

		                                @error('email')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                            </div>
		                            <!--================[]==================-->
 								
								
									

									<!--================[]==================-->	
		                           <div class="col-sm-5">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-lock"> </i>  {{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                             </div>
                             <!--================[]==================-->
								 <div class="col-sm-5">
									<div class="form-group label-floating">
								 <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><i class="fa fa-lock"> </i>  {{ __('Confirmar Password') }}</label>

			                            <div class="col-md-6">
			                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
			                            </div>
									</div>	
									</div>	
								
							<!--================[]==================-->
								<!--fieldset style="border-radius:20px;border-color:blue ; "-->
								
							
								<!--/fieldset-->
								<!--====================================-->
								
										
										
										
							<hr>
											<!--====================================-->
										<!--====================================-->
							<div class="col-sm-5">
									<div class="form-group">
									
									
										<div class="col-sm-3">
											<div class="form-group">
												<div class="">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Registrar </button>	
												</div>
											</div>
										</div>
										
										</div class="col-sm-4">
									</div class="form-group">
							<!--====================================-->
							</div>
						</form>
					</fieldset>
					</div >
    			</div>
						
			</div>
<!--include('user.modal.modalcreate')-->
					@endsection        
