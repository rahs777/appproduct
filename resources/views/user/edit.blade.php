 @extends('layouts.admin360')

@section('content')
 <div class="row">

	 	
	         @include('custom.message')
	      
        <fieldset style="border-radius:20px;box-shadow: 0 0 10px #333;padding-bottom: 10px;padding-top: 1px;padding-left:0px;margin: 30px;height:40% ;">

    
			    <div class="col-sm-12" style="border-radius:21px 20px 5px 0px;background-color:#9cadd5;color:white; ">
			        <div class="">
			            <h4>Editar usuario y Asignar Role de Usuario</h4>
			        </div>
			        
			    </div>

			    
                <form method="POST" action="{{ route('user.update',$user->id) }}" 
                       	enctype="multipart/form-data">
						@method('PUT')
									 @csrf
									 <div></div>
								<div class=" card1" style="">

										
										<!--================[]==================-->
										
										<!--================[]==================-->
							      

										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" ><i class="fa fa-user"> </i>  Name </label>
											<input class="form-control" name="name"  value="{{old('name',$user->name)}}" type="text" maxlength="100">
											</div>
											{{csrf_field()}}
										</div>
										<!--================[]==================-->
										<!--================[]==================-->
							      

										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" ><i class="fa fa-user"> </i> User Name </label>
											<input class="form-control" name="username"  value="{{old('username',$user->username)}}" type="text" maxlength="100">
											</div>
											{{csrf_field()}}
										</div>
										<!--================[]==================-->
										<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-envelope-o"> </i>  email</label>
										<input class="form-control" name="email"  value="{{old('email',$user->email)}}"   type="text">
										</div>
										</div>
										<!--====================================-->
									<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label">Rol</label>
											<select name="is_admin" id="is_admin" class="form-control" >
											
											
											<option value="1"
												@if ( $user['is_admin']  == '1')
											selected 
											@elseif( old('is_admin' ) == '1')
											  selected 
											  @endif 
											  />Admin </option>
												<option value="0"
												@if ( $user['is_admin']  == '0')
											selected 
											@elseif( old('is_admin' ) == '0')
											  selected 
											  @endif >Usuario </option>
										
											</select> 
										</div>
									</div>
									 <!--================[]==================-->
									 <!--====================================-->
									<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label">Status</label>
											<select name="status" id="status" class="form-control" >
											
											
											<option value="1"
												@if ( $user['status']  == '1')
											selected 
											@elseif( old('status' ) == '1')
											  selected 
											  @endif 
											  />Activo </option>
												<option value="0"
												@if ( $user['status']  == '0')
											selected 
											@elseif( old('status' ) == '0')
											  selected 
											  @endif >Inactivo</option>
										
											</select> 
										</div>
									</div>
									 <!--================[]==================-->
                         			<!--====================================-->
		                           <div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-lock"> </i>  {{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{old('password',$user->password)}}">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                             </div>
                             <!--================[]==================-->
								 <div class="col-sm-4">
									<div class="form-group label-floating">
									 <label for="password-confirm" class="control-label"><i class="fa fa-lock"> </i>  {{ __('Confirmar Password') }}</label>

			                           
			                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{old('password',$user->password)}}">
			                            
									</div>	
									</div>	
								
							<!--================[]==================-->
								
							<!--================[]==================-->
                        <!--================[]==================-->
								<!--====================================-->

								<!--=[muestra los roles a asignar a un usuario ]===-->
								
								<!--fieldset style="border-radius:20px;border-color:blue ; "-->

								<div class="col-sm-4">
								<div class="form-group">
								
								
								
							<hr>
									<!--====================================-->
										<div class="col-sm-6">
											<div class="form-group">
												<div class="">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Actualizar </button>	
												</div>
											</div>
										</div>
							</div>
							</div>
						
							

						</form>
					
						</fieldset>
						
					</div>

					@endsection        
