
<div id="modalusercreate" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Sign Up</h3>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> Check your mail for login confirmation!!
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10">
                    <form method="POST" id="Register" action="{{ route('user.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group label-floating" >
                                                <label class="control-label" ><i class="fa fa-user"> </i>   {{ __('Nombre') }} </label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>
                        <div class="form-group label-floating">
                            <label class="control-label" for="username" ><i class="fa fa-user"> </i>  {{ __('Nombre de Usuario') }}</label>
                                          
                                                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label"><i class="fa fa-envelope-o"> </i>  {{ __('Correo') }}</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label"><i class="fa fa-lock"> </i>  {{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group label-floating">
                             <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><i class="fa fa-lock"> </i>  {{ __('Confirmar Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                        </div>

                        <input type="hidden" name="status" value="0">
                        <div class="modal-footer">
                             <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary" id="saveBtn"
                                    value="create"> Registrar
                            </button>
                         </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>
