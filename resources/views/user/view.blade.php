 @extends('layouts.admin360')

@section('content')
 <div class="row">
 	<div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/person-plus.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Datos de Usuario </span>
             </strong>
             
             
        	</div>
	 	
	         @include('custom.message')
	      
        <fieldset >

    
			   

			    
                       <form method="POST" action="{{ route('user.update',$user->id) }}" >
						@method('PUT')
									 @csrf
									 <div></div>
								<div class=" card1" style="">
									<!--================[]==================-->
									<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label">Foto</label>
										<img width="80%" class="img-thumbnail img-avatar img-circle img-responsive"  src="{{Storage::url($user->photouser) }}">
										<input class="form-controlimg-responsive" name="photouser"  value="{{old('logohotel')}}"  type="file" alt="{{old('logohotel')}}">
										</div>
										</div>
										
										<!--================[]==================-->
							      

										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" >Name </label>
											<input class="form-control" name="name"  value="{{old('name',$user->name)}}" type="text" maxlength="100" readonly>
											</div>
											{{csrf_field()}}
										</div>

										<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label">email</label>
										<input class="form-control" name="email"  value="{{old('email',$user->email)}}"   type="text" readonly>
										</div>
										</div>

								
								
									<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label">Roles</label>
										<select name="roles" id="roles" class="form-control" disabled>
											<option value="0">Ninguno</option>
											@foreach($roles AS $role)	
											<option value="{{ $role->id }}"
												@isset($user->roles[0]->name)
													@if($role->name == $user->roles[0]->name)
													 selected
													@endif
													
												@endisset

												>{{$role->name}} </option>
											@endforeach


										</select> 
									</div>

									</div>

								<!--fieldset style="border-radius:20px;border-color:blue ; "-->

								<div class="col-sm-4">
								<div class="form-group">
									
					           
					            
											
										

										
										
								

								<!--/fieldset-->
								

								<!--/fieldset-->
								<!--====================================-->
								<!--====================================-->
								
							<hr>
									<!--====================================-->
										<div class="col-sm-6">
							                  <div class="form-group">

							                  <div class="col-sm-3">
							                        <div class="form-group">
							                      <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
							                        
							                      </div>
							                    </div>
							                    
							                    
							                    </div class="col-sm-4">
                  							</div class="form-group">
							</div>
						
							

						</form>
					
						</fieldset>
						
		</div>
	</div>
</div>
					@endsection        
