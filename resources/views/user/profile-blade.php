 @extends('layouts.dashboard')

@section('content')
 <div class="row">

	 	
	    @include('custom.message')
	      
        <fieldset style="border-radius:20px;box-shadow: 0 0 10px #333;padding-bottom: 10px;padding-top: 1px;padding-left:0px;margin: 30px;height:40% ;">

    
			    <div class="col-sm-12" style="border-radius:21px 20px 5px 0px;background-color:#9cadd5;color:white; ">
			        <div class="">
			            <h4>Asignar roles de Usuario</h4>
			        </div>
			        
			    </div>

			    
                       <form method="POST" action="{{ route('user.update',$user->id) }}" >
						@method('PUT')
									 @csrf
									 <div></div>
								<div class=" card1" style="">

										
										<!--================[]==================-->
							      		<div class="col-sm-4">
											<div class="form-group label-floating">
												<label class="control-label">Cambiar Foto</label>
												
											<img class="img-thumbnail img-avatar img-circle" width="70%" src="{{Storage::url($hotel->logohotel) }}">
											<input class="form-control" name="logohotel[]"    type="file"
											alt="{{old('logohotel')}}">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" >Name </label>
											<input class="form-control" name="name"  value="{{old('name',$user->name)}}" type="text" maxlength="100">
											</div>
											{{csrf_field()}}
										</div>

										<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label">email</label>
										<input class="form-control" name="email"  value="{{old('email',$user->email)}}"   type="text">
										</div>
										</div>

								
								
									

								<!--fieldset style="border-radius:20px;border-color:blue ; "-->

								<div class="col-sm-4">
								<div class="form-group">
									
					           
					            
											
										

										
										
								

								<!--/fieldset-->
								

								<!--/fieldset-->
								<!--====================================-->
								<!--====================================-->
								
							<hr>
									<!--====================================-->
										<div class="col-sm-6">
											<div class="form-group">
												<div class="btn btn-primary">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Actualizar Rol</button>	
												</div>
											</div>
										</div>
							</div>
						
							

						</form>
					
						</fieldset>
						
					</div>

					@endsection        
