 @extends('layouts.admin360')

@section('content')
 <div class="row">

	 	
	         @include('custom.message')
	  <div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Actualizar Datos Del Categoria</span>
             </strong>
             
             
        	</div>
	      
        <fieldset >

    
			   
			    
                       <form method="POST" action="{{ route('categoria.update',$categorium->id) }}" 
                       	enctype="multipart/form-data"
                       	>
						@method('PUT')
									 @csrf
									 <div></div>
								<div class="card1">

										
										<!--================[]==================-->
										

							      

										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" >Nombre del Producto</label>
											<input class="form-control" name="nombrecat"  value="{{old('nombrecat',$categorium->nombrecat)}}" type="text" maxlength="100">
											</div>
											{{csrf_field()}}
										</div>

								<!--====================================-->
								<div class="col-sm-4">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" >Descripcion </label>
                                            <input class="form-control" name="descripcion"  value="{{old('descripcion',$categorium->descripcion)}}" type="text" maxlength="100">
                                            </div>
                                            {{csrf_field()}}
                                        </div>
							
							<hr>
											<!--====================================-->
										<!--====================================-->
							<div class="col-sm-5">
									<div class="form-group">
									
									
										<div class="col-sm-3">
											<div class="form-group">
												<div class="">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Registrar </button>	
												</div>
											</div>
										</div>
										
										</div class="col-sm-4">
									</div class="form-group">
							<!--====================================-->
							</div>
							

						</form>
						
						</fieldset>
				</div>					
			</div>
		</div>

					@endsection        
