
<div id="modalusercreate" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Registrar Categoria</h3>
            </div>
            <form method="POST" id="Register" action="{{ route('categoria.store') }}">
                        {{ csrf_field() }}
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> Check your mail for login confirmation!!
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                        <!--================[]==================-->
                                        

                                  

                                        <div class="col-sm-4">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" >Nombre </label>
                                            <input class="form-control" name="nombrecat"  value="{{old('nombrecat')}}" type="text" maxlength="100">
                                            </div>
                                            {{csrf_field()}}
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" >Descripcion </label>
                                            <input class="form-control" name="descripcion"  value="{{old('descripcion')}}" type="text" maxlength="100">
                                            </div>
                                            {{csrf_field()}}
                                        </div>

                                                    
                                
                                    
                                <!--====================================-->
                            
                            <hr>
                                            <!--====================================-->
                                        <!--====================================-->
                           
                            <!--====================================-->
                            </div>
                        

                   
                </div>
            </div>
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
        </form>
        </div>

    </div>
</div>
