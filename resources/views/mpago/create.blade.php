 @extends('layouts.admin360')

@section('content')
 <div class="row">

	 	
	         @include('custom.message')

	<div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/cart-check.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Ingrese Tipo pagos</span>
             </strong>
         </div class="panel-heading clearfix">
        <fieldset >
        	  <form method="POST" action="{{ route('mpago.store') }}" 
        	  enctype="multipart/form-data" >
						
									 @csrf
									 <div></div>
								<div class="card1">
									
							<!--================[]==================-->
							      
										<div class="col-sm-4">
								<div class="form-group">
									
					           
					            
											<div class="control-group">
												<label class="control-label bolder blue">Tipon de pago</label>

												<div class="radio">
													<label>
														<input name="tipo" class="custom-control-input" type="radio" class="ace" value="transfAch" 
														 @if (old('tipo')=='transfAch')
											  checked
											  @endif 
											  />
														
														<label class="custom-control-label"> Tranferencia ACH</label>
													</label>
												</div>

												<div class="radio">
													<label>
														<input name="tipo" class="custom-control-input" type="radio" class="ace" value="contraentrega" 
														@if (old('tipo')=='contraentrega')
											  checked
											  @endif
											  @if (old('tipo')== null)
											  checked
											  @endif
											  />
														<label class="custom-control-label"> Contra Entrerga</label>
													</label>
												</div>

											</div>
										
									</div class="col-sm-4">
								</div class="form-group">
				                           <!--================[]==================-->
				                          {{csrf_field()}}

									
										<div class="col-sm-4">
					                        <div class="form-group label-floating" >
					                            <label class="control-label" for="username" ><i class="fa fa-leaf"> </i> {{ __('Descripcion') }}</label>
					                      
					                                <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion" autofocus>

					                                @error('descripcion')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
                       
										<!--================[]==================-->
										
									
		                            
									
								
							<hr>
											
										<!--====================================-->
							<div class="col-sm-5">
									<div class="form-group">
									
									
										<div class="col-sm-3">
											<div class="form-group">
												<div class="">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Registrar </button>	
												</div>
											</div>
										</div>
										
										</div class="col-sm-4">
									</div class="form-group">
							<!--=================[formulario]===============-->
							</div>
						</form>
					</fieldset>
					</div >
    			</div>
						
			</div>

					@endsection        
creat