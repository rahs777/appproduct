
<div id="modalusercreate" class="modal fade" role="dialog">
    <div class="modal-dialog"   style="height:80%;width:80% ;">

    <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Ingresar Metodo de Pago</h3>
            </div>
            <form method="POST" id="Register" action="{{ route('mpago.store') }}">
                        {{ csrf_field() }}
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                        <!--================[]==================-->
                                       <!--================[]==================-->
                                  
                                        <div class="col-sm-4">
                                <div class="form-group">
                                    
                               
                                
                                            <div class="control-group">
                                                <label class="control-label bolder blue">Tipon de pago</label>

                                                <div class="radio">
                                                    <label>
                                                        <input name="tipo" class="custom-control-input" type="radio" class="ace" value="transfAch" 
                                                         @if (old('tipo')=='transfAch')
                                              checked
                                              @endif 
                                              />
                                                        
                                                        <label class="custom-control-label"> Tranferencia ACH</label>
                                                    </label>
                                                </div>

                                                <div class="radio">
                                                    <label>
                                                        <input name="tipo" class="custom-control-input" type="radio" class="ace" value="contraentrega" 
                                                        @if (old('tipo')=='contraentrega')
                                              checked
                                              @endif
                                              @if (old('tipo')== null)
                                              checked
                                              @endif
                                              />
                                                        <label class="custom-control-label"> Contra Entrerga</label>
                                                    </label>
                                                </div>

                                            </div>
                                        
                                    </div class="col-sm-4">
                                </div class="form-group">
                                           <!--================[]==================-->
                                          {{csrf_field()}}

                                    
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" for="username" ><i class="fa fa-leaf"> </i> {{ __('Descripcion') }}</label>
                                          
                                                    <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" required autocomplete="descripcion" autofocus>

                                                    @error('descripcion')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                       

                                                    
                                
                                    
                                <!--====================================-->
                            
                            <hr>
                                            <!--====================================-->
                                        <!--====================================-->
                           
                            <!--====================================-->
                            </div>
                        

                   
                </div>
            </div>
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
        </form>
        </div>

    </div>
</div>
