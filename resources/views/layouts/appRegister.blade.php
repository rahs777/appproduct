<!DOCTYPE html>
<html lang="en">
<head>
	<title>360net</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	

	<link rel="icon" type="image/png" href="asset('images/icons/favicon.ico') }}"/>
<!--===============================================================================================-->
<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/vendor/bootstrap/css/bootstrap.min.css')}} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/fonts/iconic/css/material-design-iconic-font.min.css') }} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/vendor/animate/animate.css')}} ">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('loginv13/vendor/animsition/css/animsition.min.css') }} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/vendor/select2/select2.min.css')}} ">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/vendor/daterangepicker/daterangepicker.css')}} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/css/util.css')}} ">
	<link rel="stylesheet" type="text/css" href="{{asset('loginv13/css/main.css')}} ">
<!--===============================================================================================-->
</head>
<body OnLoad="document.formLogin.logemail.focus();" class="hold-transition login-page"style="background-color: #6b7b9f;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="border-radius:136px;background-image: url('img/logo_login.svg');"></div>

			<div class="wrap-login100 p-l-21 p-r-50 p-t-72 p-b-50">
				<!--///////////////////////////////////////////////////////-->
				@yield('content')
				<!--///////////////////////////////////////////////////////-->
			</div>
		</div>
	</div>
<!--===============================================================================================-->
	<script src="{{ asset('loginv13/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('loginv13/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('loginv13/vendor/bootstrap/js/popper.js') }} "></script>
	<script src="{{ asset('loginv13/vendor/vendor/bootstrap/js/bootstrap.min.js') }} "></script>
<!--===============================================================================================-->
	<script src="{{ asset('loginv13/vendor/select2/select2.min.js') }} "></script>
<!--===============================================================================================-->
	<script src="{{ asset('loginv13/vendor/daterangepicker/moment.min.js') }} "></script>
	<script src="{{ asset('loginv13/vendor/daterangepicker/daterangepicker.js') }} "></script>
<!--===============================================================================================-->
	<script src="{{ asset('loginv13/vendor/countdowntime/countdowntime.js') }} "></script>
<!--===============================================================================================-->
 <script>
      $(document).ready(function()
      {
      	
         $("#mostrarmodal").modal("show");
      });
</script>

	
 
 </body>
 </html>