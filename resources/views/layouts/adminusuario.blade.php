<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISGEPROD</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
       <!-- Animation library for notifications   -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet"/>
     <link href="{{asset('css/demo.css')}}" rel="stylesheet" />
     <link href="{{asset('css/pe-icon-7-stroke.css')}}" rel="stylesheet" />

    <!-- Bootstrap 3.3.5 
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
     <link rel="stylesheet" href="{{asset('font-awesome/4.5.0/css/font-awesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/css/material-kit.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/360net-logo-color.svg')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
 <link rel="stylesheet" href="{{asset('bootstrap/css/demo.css')}}">
 <link rel="stylesheet" href="{{asset('css/style8.css')}}">
<link rel="stylesheet" href="{{asset('css/main.css')}}">
 <link rel="stylesheet" type="text/css" href="{{asset('datatables/ccs/dataTable.bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('datatables/ccs/jquery.dataTables.min.css')}}">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/home') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SISGEPROD</b>V</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="{{asset('img/360net-logo-color.svg')}}" class="user-image" alt="User Image"></span>
        </a>
         <!-- ---------------------------------------------- -->
        <!-- Header Navbar: style can be found in header.less -->
         <!-- ---------------------------------------------- -->
    <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account: style can be found in dropdown.less -->
              <!-- User Account: style can be found in dropdown.less 

              -->
              
                               
                               
          <li class="dropdown user user-menu" style="border-radius:20px;box-shadow:0 6px 76px 0 rgb(255,255,255,0.09), 0 20px 16px 0 rgb(255,255,255,0.09); ">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!--img src="Storage::url(Auth::user()->photouser) " class="user-image" alt="User Image"-->
             <span class="hidden-xs">   {{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu" >
              <!-- User image -->
              <li class="user-header">
                <!--img class="img-thumbnail img-avatar img-circle"  src="Storage::url(Auth::user()->photouser) "-->

                <p>
                 <span class="hidden-xs"> {{ Auth::user()->name }}</span>
                 <!--$role->name small>Registrado desde  Auth::user()->created_at </small-->
                 <sm
                </p>
              </li>
              <!-- Menu Body -->
              <!--li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                 </li-->
                <!-- /.row -->
             
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href=" route('user.edit',Auth::user()->id)  " class="btn btn-default btn-flat">Profile</a>
                </div>
                  <div class="pull-right">
                    <ul class="dropdown-menu" role="menu">
                          <li>
                               <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                  {{ csrf_field() }}
                                </form>
                            </li>
                      </ul>

                      <a  class="btn btn-default btn-flat"
                         href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          {{ __('Cerrar Session') }}
                      </a>
                  </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
        <!-- ---------------------------------------------- -->
        <!-- Fin Header Navbar: style can be found in header.less -->
         <!-- ---------------------------------------------- -->
  </header>
      <!--///////////[ desde aqui el menu sidebar panel  ]////////--->
      <!-- Left side column. -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
           <div class="user-panel">
        <div class="pull-left image">
           <!--img class="img-thumbnail img-avatar img-circle"  src="Storage::url(Auth::user()->photouser) "-->
        </div>
        <div class="pull-left info">
          <p> {{Auth::user()->name }}</p>
          <p>Rol : [  isset( role->name ) role->name 
                                        endisset ]</p>
          <!--$role->name-->
           <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>

      </div>
              
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"></li>
            <li>
              <a href="#">
                <i class="fa fa-desktop"></i> <span>DashBoard Usuario</span>
                
              </a>
            </li>
            
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-hotel"></i>
                <span>Gestionar Compras</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <li><a href=" {{url('cliente/compra')}}"><i class="fa fa-circle-o" ></i>Mis Compras</a></li>
                <li><a href="{{ url('cliente/compra/create')}} "><i class="fa fa-circle-o"></i> Realizar Compra</a></li>
                
              </ul>
            </li>
           
           

           
            <li class="treeview">
              <a href="#">
                <i class="fa  fa-bell " aria-hidden="true"></i>
                <span>Pedidos</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                
                <li><a href="{{ url('cliente/pedido') }}"><i class="fa fa-circle-o"></i>listar de Pedidos</a></li>
               
                  
                 <li><a href="{{ url('cliente/pedido/create') }}"><i class="fa fa-circle-o"></i>Ingresar Pedidos</a></li>
                 
              </ul>
            </li>
            
           
            
           
            
            
            <li>
              <a href="#">
                <i class="fa fa-plus-square"></i> <span></span>
                
               
              </a>
            </li>
            
            
                       
           
            
          
          </ul>

        </section>
        <!-- /.sidebar -->
      </aside>
<!--///////////[ desde aqui el menu sidebar ]////////--->




       <!--[   Contenido  ] -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                  <div class="box-header with-border">
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      
                      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                <!-- /.box-header -->
                      <div class="box-body">
                        <div class="row">
                          <div class="col-md-12">
                              <!--Contenido-->
                              
                                @yield('content')
                                <!--Fin Contenido-->
                          </div>
                        </div>
                     </div>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.col -->
         
        
     
      <!--[   Fin-Contenido  ] -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Desarrollo WEB Ing Richard Henriquez [ Puerto la Cruz-Venezuela ]</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2020 <a href="http://rhportafolio.tk/">SIGESPROD</a>.</strong> All rights reserved.
      </footer>

      
    <!-- jQuery 2.1.4 -->
    <!--script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script-->
     <script src="{{asset('js/jquery.min.js')}}"></script>
     <script src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
    @stack('scripts')
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
     <script src="{{asset('bootstrap/js/material.min.js')}}"></script>
     <script src="{{asset('bootstrap/js/material-kit.js')}}"></script>
    
    <!-- datatables -->
    <script src="{{asset('datatables/js/jquery.dataTables.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
     
     <script src="{{asset('js/hotel360.js')}}"></script>
      <!--script src="{{asset('js/function.js')}}"></script-->
     <!-- Light Bootstrap Table DEMO methods, don't include it in your project! ->
  <script src="{{asset('js/demo.js')}} "></script>
  
   <script>
      $(document).ready(function()
      {
         $("#mostrarmodal").modal("show");
      });
</script>
    
  </body>
</html>
