<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	
	
	 <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
   <link href="{{asset('bootstrap/css/styles.css')}}" rel="stylesheet" />
     <!--FontAwesome Styles-->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
        <!-- Custom Styles-->
         <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <link  href="{{asset('css/custom-styles.css')}}"  rel="stylesheet" />
     <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
  <!-- iCheck -->
</head>
<body OnLoad="document.formLogin.logemail.focus();" class="hold-transition login-page" >
	<div class="login100-more" style=" background-image: url( '{{ asset('img/bg-01.jpg') }}' ) "></div>
<div id="login_form">
  	<div class="login-box">
  
  <!-- /.login-logo -->
        <div class="login-box-body">
		     <div class="login-logo">
		 <a href="#"><img src="{{ asset('img/360net-logo-color.svg') }}" class="user-image" alt="User Image"><b class="companyTitle"></a></b>
		  	</div>
		  	<?php
		  	//echo 'Mensajes:<strong>&nbsp;[ &nbsp '.$this->session->userdata('msg').'</strong>&nbsp;] &nbsp';
		  	?>
    		<p class="login-box-msg">Ingrese Datos para Iniciar Session De Usuario</p>
       <!--///////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-->
	
			<div class="panel-body">
			  			<!--///////////////////////[]//////-->
			  			          <!-- /. ROW  -->
			  		<div class="box">
			            <div class="content-wrap">
			                 <div class="social">
	                            <a class="face_login" href="#">
	                               	                               
	                            </a>
	                            <!--div class="division">
	                                <hr class="left">
	                                <span>or</span>
	                                <hr class="right">
	                            </div-->
	                        </div>
	                        </div>
	                        </div>
            	<!--div class="row"-->

                	<div class="col-md-8">
                    	<div class="form-group">
                    		<fieldset style="border-radius: 20px;box-shadow: 0 0 10px #333;padding-top:30px;padding-bottom:80px;padding-left:-8px;padding-right:30px ; margin: 10px;width:160%; ">
                      
                        <!--div class="row"-->
                                   	
                                <div class="">
                                   		 @yield('content')
				           		</div>
                			
			  			<!--///////////////////////[ ]/////-->
			  		</fieldset>
						</div>
			  		</div>
	  	        <!--/div--->
	        </div>
        </div>
    </div>
</div>
 
  

</body>
 <!-- jQuery 2.1.4 
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>-->
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
     
      <!-- Custom Js -->
 <script src="{{asset('js/custom-scripts.js')}}"></script>
<!-- 

-->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"> </script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"> </script>
<script src="{{asset('datatables/js/jquery.dataTables.min.js')}}"> </script>
<script src="{{asset('datatables/js/dataTables.bootstrap.js')}}"> </script>
<script src="{{asset('bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
 