@extends('layouts.appRegister')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             <div class="login-logo">
         <a href="#"><img src="{{ asset('img/360net-logo-color.svg') }}" style="width:70%;" class="user-image" alt="User Image"><b class="companyTitle"></a></b>
            </div>
             <span class="login100-form-title p-b-15">
                        Iniciar Session
                    </span>
            <div class="card">
                 @include('custom.message')
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                    <label for="usaname" class=" col-form-label text-md-right"></label>
                        <div class="form-group" style="">
                            <div class="col-sm-12">
                                <input id="usarname" type="text" class="form-control @error('usarname') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="USUARIO" required autocomplete="usarname" autofocus style="width:100%;border-radius:20px 20px 20px 20px;box-shadow:1px 4px 4px rgba(56, 81, 128, 1);">

                                    @error('usarname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                               
                            </div>
                        </div>

                    <label for="password" class="col-form-label text-md-right"></label>
                        <div class="form-group" style="margin-bottom: 39px;">
                            <div class="col-sm-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" style="width:100%;border-radius:20px 20px 20px 20px;box-shadow:1px 4px 4px rgba(56, 81, 128, 1);" placeholder="PASSWORD">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                 
                        <div class="form-group row">
                            <div class="col-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button type="submit" class="login100-form-btn">
                               <i class="fa fa-send"></i>  {{ __('Login') }} 
                            </button>
                        </div>
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button type="submit" class="login100-form-btn btn btn-info btn btn-success" >
                                @if (Route::has('register'))
                                       
                                            <a  href="{{ route('register') }}"><i class="fa fa-sign-out "></i> {{ __('Registro Usuario') }}</a>
                                       @endif
                            </button>
                        </div>
                        <!--//////////////////////////////-->
                          <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button type="submit" class="login100-form-btn btn btn-default btn btn-success" class="">
                             @if (Route::has('password.request'))
                            <a class="" href="{{ route('password.request') }}">
                                   <i class="fa fa-lock"></i>  {{ __('Forgot Your Password?') }}
                                     </a>
                             @endif
                              </button>
                         </div>
                        <!--//////////////////////////////-->

                        
                        
                    </div>

                         <!--div class="form-group row">
                            <div class="col-sm-5">
                           
                               
                               
                                <div class="col-sm-4 ">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Login') }}
                                        </button>
                                   </div>
                                </div-->
                                 <!--//////////////////////////////-->
                                  <!--div class="col-sm-4  ">
                                    <div class="form-group ">

                                        @if (Route::has('register'))
                                       
                                            <a class="btn btn-default" href="{{ route('register') }}">{{ __('Registro Huespedes') }}</a>
                                       @endif
                                   </div>   
                                </div>

                                
                                
                               
                                 
                            </div>
                        </div-->
                        <!--//////////////////////////////-->
                        <!--div class="col-sm-2">
                            <div class="form-group ">
                               
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-success" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                     </a>
                                    @endif
                            </div>
                        </div-->
                        <!--//////////////////////////////-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
