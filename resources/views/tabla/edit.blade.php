 @extends('layouts.admin360')

@section('content')
 <div class="row">
	         @include('custom.message')
	  <div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Actualizar Tabla </span>
             </strong>
             
             
        	</div>
	      
        <fieldset >

                       <form method="POST" action="{{ route('tabla.update',$tabla->id) }}" 
                       	enctype="multipart/form-data"
                       	>
						@method('PUT')
									 @csrf
									 <div></div>
								<div class="card1">
									<!--================[]==================-->
							      
										<div class="col-sm-5">
											<div class="form-group label-floating" >
												<label class="control-label" ><i class="fa fa-user"> </i>   {{ __('Stock Minimo') }} </label>
				                                <input id="stockmin" type="number" class="form-control @error('stockmin') is-invalid @enderror" name="stockmin" value="{{ old('stockmin',$tabla->stockmin) }}" required autocomplete="stockmin" autofocus>

				                                @error('stockmin')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                          </div>
				                         
				                          </div>
				                           <!--================[]==================-->
				                          {{csrf_field()}}

									
										<div class="col-sm-5">
					                        <div class="form-group label-floating" >
					                            <label class="control-label" for="username" ><i class="fa fa-user"> </i>  {{ __('IVA') }}</label>
					                      
					                                <input id="iva" type="number" class="form-control @error('iva') is-invalid @enderror" name="iva" value="{{ old('iva',$tabla->iva) }}" required autocomplete="iva" autofocus>

					                                @error('iva')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
                       
										<!--================[]==================-->
									<div class="col-sm-5">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-envelope-o"> </i>  {{ __('Monto Minimo Para Compras') }}</label>
		                                <input id="montominimocompra" type="number" class="form-control @error('montominimocompra') is-invalid @enderror" name="montominimocompra" value="{{ old('montominimocompra',$tabla->montominimocompra) }}" required autocomplete="montominimocompra">

		                                @error('montominimocompra')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                            </div>
		                            <!--================[]==================-->
											
										<!--====================================-->
							<div class="col-sm-5">
									<div class="form-group">
									
									
										<div class="col-sm-3">
											<div class="form-group">
												<div class="">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Actualizar </button>	
												</div>
											</div>
										</div>
										
										</div class="col-sm-4">
									</div class="form-group">
							<!--=================[formulario]===============-->
							</div>
						
							

						</form>
						
						</fieldset>
				</div>					
			</div>
		</div>

					@endsection        
