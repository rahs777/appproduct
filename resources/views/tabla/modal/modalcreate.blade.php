
<div id="modalusercreate" class="modal fade" role="dialog">
    <div class="modal-dialog"   style="height:80%;width:80% ;">

    <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Ingresar Producto</h3>
            </div>
            <form method="POST" id="Register" action="{{ route('tabla.store') }}">
                        {{ csrf_field() }}
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                        <!--================[]==================-->
                                  
                                        <div class="col-sm-5">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" ><i class="fa fa-user"> </i>   {{ __('Stock Minimo') }} </label>
                                                <input id="stockmin" type="number" class="form-control @error('stockmin') is-invalid @enderror" name="stockmin" value="{{ old('stockmin') }}" required autocomplete="stockmin" autofocus>

                                                @error('stockmin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>
                                         
                                          </div>
                                           <!--================[]==================-->
                                          {{csrf_field()}}

                                    
                                        <div class="col-sm-5">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" for="username" ><i class="fa fa-user"> </i>  {{ __('IVA') }}</label>
                                          
                                                    <input id="iva" type="number" class="form-control @error('iva') is-invalid @enderror" name="iva" value="{{ old('iva') }}" required autocomplete="iva" autofocus>

                                                    @error('iva')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                       
                                        <!--================[]==================-->
                                    <div class="col-sm-5">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-envelope-o"> </i>  {{ __('Monto Minimo Para Compras') }}</label>
                                        <input id="montominimocompra" type="number" class="form-control @error('montominimocompra') is-invalid @enderror" name="montominimocompra" value="{{ old('montominimocompra') }}" required autocomplete="montominimocompra">

                                        @error('montominimocompra')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    </div>
                                    <!--================[]==================-->
                                                    
                                
                                    
                                <!--====================================-->
                            
                            <hr>
                                            <!--====================================-->
                                        <!--====================================-->
                           
                            <!--====================================-->
                            </div>
                        

                   
                </div>
            </div>
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
        </form>
        </div>

    </div>
</div>
