
@extends('layouts.admin360')

@section('content')
<div class="row">
     <div class="col-md-12">
       @include('custom.message')
     </div>
  <div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; ">
                <span ><img src="{{asset('img/bootstrapicons/people.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Ver Setup de Productos y Pedidos</span>
             </strong>
             
             <div class="pull-right">
                <!--a type="button"  class="btn btn-info"  href="{{route('user.create')}}"> <i class="glyphicon glyphicon-edit"></i> Nuevo Usuario</a-->
                 <a type="button" href="javascript:void(0)" class="btn btn-success " id="new-user" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Nuevo </a>
                @include('tabla.modal.modalcreate')
             </div>
            
        </div>

      <div class="panel-body">
             
          

        <div class="card-body">
                      <div class="col-xs-15">
                       

                      <div id="row">

                         
                           <!---id="datatable360"-->
                        <table   class="table table-hover table-bordered table-responsive" >
                         
                        <thead class="nav nav-tabs active" style="color:white;" >
                                  <tr>
                                    <th><input type="checkbox" class="check-all"></th>
                                      <th><center>ID</center></th>
                                      <th><center>Stock</center></th>
                                      <th><center>iva</center></th>
                                      <th><center>Monto Minimo Compra</center></th>
                                       <th><center>Fecha Registro</center></th>
                                      
                                      <th ><center>Acciones</center></th>
                                  </tr>
                              </thead>
                             
                              <tbody>
                              </tbody>
                              <thead>
                           
                          @foreach($tablas as $tabla)
                          
                          <tr >
                             <div class="opcion">
                             <th><center>

                              <input type="checkbox" class="data-check" value="{{ $tabla->id }}" /> </th>
                                          <th>{{ $tabla->id}}</th>
                                         <th>{{ $tabla->stockmin}}</th>
                                      <th>{{ $tabla->iva }}</th>
                                      <th>{{ $tabla->montominimocompra }}</th>
                                       
                                       
                                       


                                      
                                      
                                       
                                         <!--th>if ($user->status == '1' )
                                          Activo
                                          else 
                                           Inactivo
                                         endif</th-->
                                      <th>{{ $tabla->created_at }}</th>
                                      
                                      <td class="text-center">
                             
                                         </div>

                             <!--====================[]============================-->
  
              <div class="btn-group">
              <button type="button" class="btn  dropdown-toggle bg-blue" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Acción <span class="caret"></span>
              </button>
              
              <!--====================[ Show ]============================-->
              <ul class="dropdown-menu">
                <!--====================[Edit]============================-->
                <!--li>
                  <button class="btn btn-info"><a href="{{ route('tabla.show',$tabla->id)}}" ><i class="glyphicon glyphicon-user"></i>Show </a>
                  </button>
                 </li-->
                <!--====================[ Edit ]============================-->
                <li>
                <button class="btn btn-success">
                  <a href="{{ route('tabla.edit',$tabla->id)  }}" ><i class="glyphicon glyphicon-trash"></i>Edit </a>
                </button>
               
                </li>
                <!--====================[ DElete ]============================-->
                  <li>
                    <!--div class="col-sm-3"-->  
                    <!--button class="btn btn-danger ">        
                      <a href="#" data-target="#modal-userdelete-{{$tabla->id}}" data-toggle="modal">
                      <i class="glyphicon glyphicon-trash"></i>  Delete
                                  </a>
                                  include('tabla.modal.modaluserdel')
                    </button-->
                                 
                              <!--div--> 
                    <form action="{{route('tabla.destroy',$tabla->id)}}" method="POST"> 
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger">Delete</button>
                    </form>

                    
                 </li> 
                <!--=================[]=======================--data-target="#delete_"--> 
                </ul>
              </div>
           
            <!--====================[]============================-->
 
              </div>
            </div>
            <!--====================[]============================-->
                </td>  
                                     
          </tr>
        @endforeach
                       
                                  
                              </thead>
                           
                         </table>
                          <div class="form-group"> 
                          <div class="col-sm-4">
                           <div class="form-group">
                              {{ $tablas->links() }}  
                           </div>
                           </div>
                          

                        </div>

                    </div>
                    
                           <!--====================================-->
                <div class="col-sm-6">
                  <div class="form-group">

                 
                    
                    
                    </div class="col-sm-4">
                  </div class="form-group">
              <!--====================================-->
                  </div>
                   
        </div><!--card-body-->
      </div><!--panel-body-->
    </div><!--panel panel-info-->
  </div><!--col-md-12-->
</div><!--row-->

 @include('tabla.modal.modalcreate')
@endsection
<!---

-->


