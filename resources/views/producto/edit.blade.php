 @extends('layouts.admin360')

@section('content')
 <div class="row">
	         @include('custom.message')
	  <div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Actualizar Producto </span>
             </strong>
             
             
        	</div>
	      
        <fieldset >

                       <form method="POST" action="{{ route('producto.update',$producto->id) }}" 
                       	enctype="multipart/form-data"
                       	>
						@method('PUT')
									 @csrf
									 <div></div>
								<div class="card1">
									<!--================[]==================-->
							      
										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" ><i class="fa fa-glass"> </i> {{ __('Nombre') }} </label>
				                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre',$producto->nombre )}}" required autocomplete="nombre" autofocus>

				                                @error('nombre')
				                                    <span class="invalid-feedback" role="alert">
				                                        <strong>{{ $message }}</strong>
				                                    </span>
				                                @enderror
				                          </div>
				                         
				                          </div>
				                           <!--================[]==================-->
				                          {{csrf_field()}}

									
										<div class="col-sm-4">
					                        <div class="form-group label-floating" >
					                            <label class="control-label" for="username" ><i class="fa fa-leaf"> </i> {{ __('Precio') }}</label>
					                      
					                                <input id="precio" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio',$producto->precio) }}" required autocomplete="precio" autofocus>

					                                @error('precio')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
                       
										<!--================[]==================-->
										<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Categoria') }}</label>
										<select name="categoria" id="categoria" class="form-control" >
											
											@foreach ($categorias AS $categoria)
											 <option value="{{ $categoria->nombrecat, $categoria->id }}"> {{ $categoria->nombrecat }}</option>

											
											@endforeach


										</select> 
									</div>

									</div>

									
		                            
									
								<!--====================================-->
								<!--================[]==================-->
									<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-book "> </i> {{ __('Stock') }}</label>
		                                <input id="stock" type="stock" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock',$producto->stock) }}" required autocomplete="stock">

		                                @error('stock')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                            </div>
		                            
									
								<!--====================================-->
		                           <div class="col-sm-4">
										<div class="form-group label-floating">
													<label class="control-label"><i class="fa fa-lock"> </i> {{ __('Descripcion') }}</label>
		                                <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion"  value="{{ old('descripcion',$producto->descripcion) }}" required autocomplete="new-descripcion">

		                                @error('descripcion')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
                            			</div>
                             		</div>
                             <!--================[]==================-->
								
								
						
										
							<hr>
											
										<!--====================================-->
							<div class="col-sm-5">
									<div class="form-group">
									
									
										<div class="col-sm-3">
											<div class="form-group">
												<div class="">
													<button class="btn btn-primary" type="submit" name="save" value="envio"> <span class="glyphicon glyphicon-save"></span> Actualizar </button>	
												</div>
											</div>
										</div>
										
										</div class="col-sm-4">
									</div class="form-group">
							<!--=================[formulario]===============-->
							</div>
						
							

						</form>
						
						</fieldset>
				</div>					
			</div>
		</div>

					@endsection        
