
@extends('layouts.admin360')

@section('content')
<div class="row">
     <div class="col-md-12">
       @include('custom.message')
     </div>
  <div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; ">
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">lista de Productos</span>
             </strong>
             
             <div class="pull-right">
                <!--a type="button"  class="btn btn-info"  href="{{route('producto.create')}}"> <i class="glyphicon glyphicon-edit"></i> Nuevo </a-->
                 <a type="button" href="javascript:void(0)" class="btn btn-success " id="new-user" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Nuevo </a>
                @include('producto.modal.modalcreate')
             </div>
            
        </div>

      <div class="panel-body">
        <div class="card-body">
          <div class="col-xs-15">
            <!--====================================--> 
              <div id="row">
                      <table id="datablehoteles" class="table table-hover " >
                        <thead class="nav nav-tabs active" style="color:white;">
                                  <tr>
                                    <th></th>
                                      <th><center>ID</center></th>
                                      
                                      <th><center>Nombre</center></th>
                                      <th><center>Precio</center></th>
                                      <th><center>Categoria</center></th>
                                      <th><center>stock</center></th>
                                      <th><center>Descripcion</center></th>
                                      <!--th><center>Stock minimo </center></th-->
                                     <th ><center>Acciones</center></th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                              <thead>
                             <tr>
                          @foreach($productos as $producto)
                          
                          <tr>
                             <th><center><input type="checkbox" class="data-check" value="{{ $producto->id }}" onclick="showBottomDelete()"/> </th>
                                     
                                      <th>{{ $producto->id }}</th>
                                       <th>{{ $producto->nombre }}</th>
                                       <th>{{ $producto->precio }}</th>
                                       <th>{{ $producto->categoria }}</th>
                                       <th>{{ $producto->stock }}</th>
                                       <th>{{ $producto->descripcion }}</th>
                                       
                                      
                                      <td class="text-center">

                        <!--===[ dropdown Menu ]==-->
          

                <div class="btn-group">
                    <button type="button" class="btn  dropdown-toggle bg-blue" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Acción <span class="caret"></span>
                    </button>
                    
                   <!--====================[]============================-->
                    <ul class="dropdown-menu">
                     <!--====================[Edit]============================-->
                      <!--li>
                        <button class="btn btn-info"><a d  href="{{ route('producto.show',$producto->id)}}"> <i class="glyphicon glyphicon-user"></i>  Show</a></button >
                      </li-->
                      <!--====================[ Edit ]============================-->
                      <li>
                        <button class="btn btn-success"> <a    href="{{ route('producto.edit',$producto->id)  }}"> <i class="glyphicon glyphicon-edit"></i>  Edit</a></button >
                      </li>
                      <!--====================[ DElete ]============================-->
                      <li>
                        <form action="{{route('producto.destroy',$producto->id)}}" method="POST"> 
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-danger "><i class="glyphicon glyphicon-trash"></i>  Delete</button>
                        </form>
                     </li> 
                  </ul>
                </div>
                   <!--===[ dropdown Menu ]==-->
                        <!--====================[]============================-->
                    </td>
                  </tr>
                @endforeach
                      <div class="form-group"> 
                          <div class="col-sm-4">
                           <div class="form-group">
                              {{ $productos->links() }}  
                           </div>
                           </div>
                        </div>

                                  
                </thead>
              </table>
             </div><!--row--->
                    <!--====================================-->
                      <div class="col-sm-6">
                          <div class="form-group">

                          <div class="col-sm-3">
                                <div class="form-group">
                              <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
                                
                              </div>
                            </div>
                            
                            
                            </div class="col-sm-4">
                        </div class="form-group">
              <!--====================================-->
            </div>
                   
        </div><!--card-body-->
      </div><!--panel-body-->
    </div><!--panel panel-info-->
  </div><!--col-md-12-->
</div><!--row-->


@endsection

@section ('js_user_page')

@endsection