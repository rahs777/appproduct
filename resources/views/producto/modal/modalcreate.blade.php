
<div id="modalusercreate" class="modal fade" role="dialog">
    <div class="modal-dialog"   style="height:80%;width:80% ;">

    <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Ingresar Producto</h3>
            </div>
            <form method="POST" id="Register" action="{{ route('producto.store') }}">
                        {{ csrf_field() }}
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                        <!--================[]==================-->
                                        

                                  

                                       
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" ><i class="fa fa-glass"> </i> {{ __('Nombre') }} </label>
                                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                                @error('nombre')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>
                                         
                                          </div>
                                           <!--================[]==================-->
                                          {{csrf_field()}}

                                    
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating" >
                                                <label class="control-label" for="username" ><i class="fa fa-leaf"> </i> {{ __('Precio') }}</label>
                                          
                                                    <input id="precio" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio') }}" required autocomplete="precio" autofocus>

                                                    @error('precio')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                       
                                        <!--================[]==================-->
                                        <div class="col-sm-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Categoria') }}</label>
                                        <select name="categoria" id="categoria" class="form-control" >
                                            
                                            @foreach ($categorias AS $categoria)
                                             <option value=" {{ $categoria->nombrecat, $categoria->id }} "> {{ $categoria->nombrecat }}</option>

                                            
                                            @endforeach
                                            

                                        </select> 
                                    </div>

                                    </div>

                                    
                                    
                                    
                                <!--====================================-->
                                <!--================[]==================-->
                                    <div class="col-sm-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-book "> </i> {{ __('Stock') }}</label>
                                        <input id="stock" type="stock" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock') }}" required autocomplete="stock">

                                        @error('stock')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    </div>
                                    
                                    
                                <!--====================================-->
                                   <div class="col-sm-4">
                                        <div class="form-group label-floating">
                                                    <label class="control-label"><i class="fa fa-lock"> </i> {{ __('Descripcion') }}</label>
                                        <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion"  value="{{ old('descripcion') }}" required autocomplete="new-descripcion">

                                        @error('descripcion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                    </div>

                                                    
                                
                                    
                                <!--====================================-->
                            
                            <hr>
                                            <!--====================================-->
                                        <!--====================================-->
                           
                            <!--====================================-->
                            </div>
                        

                   
                </div>
            </div>
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
        </form>
        </div>

    </div>
</div>
