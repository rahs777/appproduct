 @extends('layouts.admin360')

@section('content')
 <div class="row">

	 	
	         @include('custom.message')
	  <div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Ver Producto </span>
             </strong>
             
             
        	</div>
	      
        <fieldset >

    
			   
			    
                       <form method="POST" action="" >
						
									 @csrf
									 <div></div>
								<div class="card1">

										
									<!--================[]==================-->
							      
										<div class="col-sm-4">
											<div class="form-group label-floating" >
												<label class="control-label" ><i class="fa fa-glass"> </i> {{ __('Nombre') }} </label>
				                                <input id="nombre" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio"  value="{{ old('nombre',$producto->nombre) }}" readonly>

				                               
				                          </div>
				                         
				                          </div>
				                           <!--================[]==================-->
				                          {{csrf_field()}}

									
										<div class="col-sm-4">
					                        <div class="form-group label-floating" >
					                            <label class="control-label" for="username" ><i class="fa fa-leaf"> </i> {{ __('Precio') }}</label>
					                      
					                                <input id="precio" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio',$producto->precio) }}"  readonly>

					                              
					                            </div>
					                        </div>
                       
										<!--================[]==================-->
										<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Categoria') }}</label>
											<input class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ $producto->categoria}}"> 

											
											

										</select> 
									</div>

									</div>

									
		                            
									
								<!--====================================-->
								<!--================[]==================-->
									<div class="col-sm-4">
										<div class="form-group label-floating">
											<label class="control-label"><i class="fa fa-book "> </i> {{ __('Stock') }}</label>
		                                <input id="stock" type="stock" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock',$producto->stock) }}" readonly>

		                                
		                            </div>
		                            </div>
		                            
									
								<!--====================================-->
		                           <div class="col-sm-4">
										<div class="form-group label-floating">
													<label class="control-label"><i class="fa fa-lock"> </i> {{ __('Descripcion') }}</label>
		                                <input id="descripcion" type="text"  class="form-control @error('precio') is-invalid @enderror" name="precio"  value="{{ old('descripcion',$producto->descripcion) }}" readonly>

		                                
                            			</div>
                             		</div>
                             <!--================[]==================-->
							<hr>
									<!--====================================-->
										
										
							<hr>
											<!--====================================-->
										<!--====================================-->
							 <div class="col-sm-6">
				                  <div class="form-group">

				                  <div class="col-sm-3">
				                        <div class="form-group">
				                      <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
				                        
				                      </div>
				                    </div>
				                    
				                    
				                    </div class="col-sm-4">
                  			</div class="form-group">
							<!--====================================-->
							</div>
						
							

						</form>
						
						</fieldset>
				</div>					
			</div>
		</div>

					@endsection        
