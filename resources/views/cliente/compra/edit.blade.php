@extends('layouts.adminusuario')
<!---usuario-->
@section('content')
 <div class="row">
  @include('custom.message')

	<div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/cart-check.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Actuaalizar Datos </span>
             </strong>
         </div class="panel-heading clearfix">
        <fieldset>
        <form method="POST" id="Register" action="{{ route('clientecompra.update',$compra->id) }}"enctype="multipart/form-data"
                       	>
                    @method('PUT')
									 @csrf
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                        <!--================[]==================-->
                                        
<!--================[]==================-->
                                        <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Cliente') }}</label>
                                        <select name="cliente" id="cliente" value" {{ old('cliente',$compra->cliente) }}" class="form-control" >
                                            
                                            @foreach ($users AS $user)
                                             <option value="{{ $user->name, $user->id }}"> {{ $user->name }}</option>

                                            
                                            @endforeach


                                        </select> 
                                    </div>

                                    </div>

                                    
                                    <input type="hidden" id="stockmin" value="{{ $stockmin->stockmin }}"></div>
                            
                            <!--================[]==================-->
                                        <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Producto') }}</label>
                                        <select name="producto" id="producto" value" {{ old('producto',$compra->producto) }}" class="form-control" >
                                            
                                            @foreach ($productos AS $producto)
                                             <option value="{{ $producto->id }}"> {{ $producto->nombre }}</option>

                                            
                                            @endforeach


                                        </select> 
                                    </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Precio') }}</label>
                                            
                                             <input id="precio" type="number" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio',$compra->precio) }}" required autocomplete="precio" autofocus>
                                             </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Cantidad') }}</label>
                                            
                                             <input id="cantidad" type="number" class="form-control @error('cantidad') is-invalid @enderror" name="cantidad" value="{{ old('cantidad',$compra->cantidad ) }}" required autocomplete="cantidad" autofocus>

                                    </div>
                                        
                                    
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Total Factura') }}</label>
                                            
                                             <input id="totalcompra" type="text" class="form-control @error('totalcompra') is-invalid @enderror" name="totalcompra" value="{{ old('nfacttotalcompraura,$compra->totalcompra') }}" required autocomplete="totalcompra" autofocus>
                                             </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('No. Factura') }}</label>
                                            
                                             <input id="nfactura" type="text" class="form-control @error('nfactura') is-invalid @enderror" name="nfactura" value="{{ old('nfactura,$compra->nfactura') }}" required autocomplete="nfactura" autofocus>
                                             </div>

                                    </div>
                                    
                                    
                                    
                                <!--====================================-->
                            <!--=============[]===============-->
                            
                            <!--=============[]===============-->
                        </div>
                        <!--=======================================-->
                        <div class="form-group">
                            <!--=============[]===============-->
                        
                            <!--=============[]===============-->
                            
                            <!--=============[]===============-->
                            <div class="col-md-3">
                                <div class="form-group">
                                <label for="email" class="">Pago</label>
                                
                                    <select name="pago" id="pago" class="form-control" >
                                            <option value="transfAch">Transferencia por ACH</option>
                                            <option value="contraentrega">Contra Entrega</option>
                                            <!--foreach ($mpagos AS $mpago)
                                             <option value=" $mpago->tipo, $mpago->id ">  $mpago->descripcion </option>

                                            
                                            endforeach-->


                                        </select> 
                                
                            </div>
                            </div>
                            
                        </div>
                        
                                <!--====================================-->
                            
                            <hr>
                                            <!--====================================-->
                                        <!--====================================-->
                           
                            <!--====================================-->
                            </div>
                        

                   
                </div>
            
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
            
        </form>
					
				</fieldset>
					</div>
    			</div>
						
			</div>

					@endsection        
