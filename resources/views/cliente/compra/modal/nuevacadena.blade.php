<div class="modal fade" id="modalnuevacadena" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            <!--================[ contenido ]==================-->
              <!--================[ contenido ]==================-->
            <div class="row">
     @include('custom.message')
    <div class="col-md-12">
        <div class="panel panel-info" >
            <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;" >
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Nueva Cadena </span>
             </strong>
            </div>
        <div class="panel-body" style="padding:0px;">
         
        <form method="POST" name="formNuevacadena" action="{{ route('hotel.storecadenas') }}" 
                       enctype="multipart/form-data"
                       >
                       {{csrf_field()}}
           
                <!--================[                     ]==================-->
                <!--================[ tab Registrar Hotel ]==================-->
               
                <div class="row">
                        <fieldset > @csrf
                                     <div></div>
                            <div class="card1">
                                        

                                            <!--===[]===-->

                                            <div class="form-group label-floating" id="cadenayes" style="display:none;">
                                                <label class="control-label" >Nombre Cadena</label>
                                            <input id="name" type="text" class="form-control @error('namesl') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>
                                            </div>
                                            <!--===[]===-->

                                        </div>
                                        <!--================[]==================-->
                                    
                                        
                    </fieldset>
                </div>
                            
                                <!--====================================-->
                   
                                <!--====================================-->
            </div> 
            
             <!--================[ Fin contenido] ==================-->
       
       
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                    <button type="sumit" id="saveBtn" class="btn btn-primary" >Save</button>
                </div>
            </div>
   </div>
  </div>
</div>  