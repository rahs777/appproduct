 
<!--script type="text/javascript">
	alert('mensaje');
</script-->

@if(session('status_error'))

<!--p data-toggle="modal" data-target="#myModal">Open Modal</p>
Trigger Via JavaScript-->
<div class="modal fade" id="mostrarmodal" tabindex="-1″ role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-header">
				<h4>Resultado es</h4>
				</div>

				

  <div class="modal-body">
					<div class="alert alert-danger alert-dismissible" style="border-radius:20px 20px 20px 20px;width:100%">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Success!</strong> {{ session('status_error') }}
				</div>
			</div>

				<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn btn-danger">Cerrar</a>
				</div>
		</div>
	</div>
</div>

	 
  
<!--div class="alert alert-success alert-dismissible" style="border-radius:20px 20px 20px 20px;width:100%">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Success!</strong> {{ session('status_success') }}
</div-->

	<!--div id="status_success" title="Dia" style="border-radius:20%;width:25%" class="alert  alert-success" role="alert">
	{{ session('status_success') }}
	</div-->
@endif
@if(session('status_success'))

<!--p data-toggle="modal" data-target="#myModal">Open Modal</p>
Trigger Via JavaScript-->
<div class="modal fade" id="mostrarmodal" tabindex="-1″ role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-header">
				<h4>Resultado es</h4>
				</div>

				<div class="modal-body">
					<div class="alert alert-success alert-dismissible" style="border-radius:20px 20px 20px 20px;width:100%">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Success!</strong> {{ session('status_success') }}
				</div>
			</div>
				
				<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn btn-danger">Cerrar</a>
				</div>
		</div>
	</div>
</div>

	 
  
<!--div class="alert alert-success alert-dismissible" style="border-radius:20px 20px 20px 20px;width:100%">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Success!</strong> {{ session('status_success') }}
</div-->

	<!--div id="status_success" title="Dia" style="border-radius:20%;width:25%" class="alert  alert-success" role="alert">
	{{ session('status_success') }}
	</div-->
@endif

@if ($errors->any())
<div class="modal fade" id="mostrarmodal" tabindex="-1″ role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-header">
				<h4>Resultado es</h4>
				</div>
				<div class="alert alert-danger alert-dismissible" style="border-radius:20px 20px 20px 20px;width:100%">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Danger!</strong> @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
			</div>
				<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn btn-danger">Cerrar</a>
				</div>
		</div>
	</div>
</div>


<!--div class="alert alert-danger">
  <strong>Danger!</strong> @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
</div-->
	      <!--div id="status_success" style="border-radius:20px;" class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	      </div--><br />
	        @endif