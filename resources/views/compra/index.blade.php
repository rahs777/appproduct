
@extends('layouts.admin360')

@section('content')
<div class="row">
     
       @include('custom.message')
    
  <div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; ">
                <span ><img src="{{asset('img/bootstrapicons/shop-window0.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">lista Compras</span>
             </strong>
             
             <div class="pull-right">
                <!--a type="button"  class="btn btn-info"  href="{{route('categoria.create')}}"> <i class="glyphicon glyphicon-edit"></i> Nuevo </a-->

                <a type="button" href="javascript:void(0)" class="btn btn-success " id="new-user" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i> Nueva Compra </a>
                @include('compra.modal.modalcreate')
             </div>
            
        </div>

      <div class="panel-body">
        <div class="card-body">
          <div class="col-xs-15">
            <!--====================================--> 
              <div id="row">
                      <table id="datTable" class="table table-hover " >
                        <thead class="nav nav-tabs active" style="color:white;">
                                  <tr>
                                    <th><input type="checkbox" id="check-all"></th>
                                      <th><center>ID</center></th>
                                     
                                      <th><center>Cliente</center></th>
                                      <th><center>Producto</center></th>
                                      <th><center>Cantidad</center></th>
                                     <th><center>Precio</center></th>
                                     <th><center>Total Compra</center></th>
                                     <th><center>Factura</center></th>
                                      <th><center>Fecha Registro</center></th>
                                      <th ><center>Acciones</center></th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                              <thead>
                             <tr>
                          @foreach($compras as $compra)
                          
                          <tr>
                             <th><center><input type="checkbox" class="data-check" value="{{ $compra->id }}" onclick="showBottomDelete()"/> </th>
                                     
                                       <th>{{ $compra->id }}</th>
                                        
                                       <th>{{ $compra->cliente }}</th>
                                       <th>{{ $compra->producto }} 
                                        </th>
                                       <th>{{ $compra->cantidad }}</th>
                                       <th>{{ $compra->precio }}</th>
                                       <th>{{ $compra->totalcompra }}</th>
                                       <th>{{ $compra->nfactura }}</th>
          
                                       <th>{{ $compra->created_at }}</th>
                                      
                                      <td class="text-center">

 <!--====================[]============================-->
          

                  <div class="btn-group">
                    <button type="button" class="btn  dropdown-toggle bg-blue" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Acción <span class="caret"></span>
                    </button>
                    
                   <!--====================[]============================-->
                    <ul class="dropdown-menu">
                     <!--====================[Edit]============================-->
                    
                      <!--====================[ Edit ]============================-->
                      <li>
                        <button class="btn btn-success"> <a   type="button" class="btn btn-success " href="{{ route('compra.edit',$compra->id)  }}"> <i class="glyphicon glyphicon-edit"></i>  Editar</a></button >
                      </li>
                      <!--====================[ DElete ]============================-->
                      <li>
                        <form action="{{route('compra.destroy',$compra->id)}}" method="POST"> 
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-danger "><i class="glyphicon glyphicon-trash"></i>  Borrar</button>
                        </form>
                     </li> 
                  </ul>
                      <!--=================[]=======================--> 
                  </div>
          
                        <!--====================[]============================-->
                    </td>
                  </tr>
                @endforeach
                     

                                  
                </thead>
              </table>
               <div class="form-group"> 
                          <div class="col-sm-4">
                           <div class="form-group">
                              {{ $compras->links() }}  
                           </div>
                           </div>
                        </div>
             </div><!--row--->
                    <!--====================================-->
                      <div class="col-sm-6">
                          <div class="form-group">

                          <div class="col-sm-3">
                                <div class="form-group">
                              <a href="{{ URL::previous() }}" class="btn btn-info">Back</a>
                                
                              </div>
                            </div>
                            
                            
                            </div class="col-sm-4">
                        </div class="form-group">
              <!--====================================-->
            </div>
                   
        </div><!--card-body-->
      </div><!--panel-body-->
    </div><!--panel panel-info-->
  </div><!--col-md-12-->
</div><!--row-->


@endsection

@section ('js_user_page')

@endsection