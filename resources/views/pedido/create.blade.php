@extends('layouts.admin360')
<!---usuario-->
@section('content')
 <div class="row">
  @include('custom.message')

	<div class="col-md-12">
    	<div class="panel panel-info" >
	        <div class="panel-heading clearfix" style="border-top-left-radius: 37px;
border-top-right-radius: 33px;background-color: #6a8c9b;">
              <strong style="padding:20px; "  >
                <span ><img src="{{asset('img/bootstrapicons/cart-check.svg')}}" alt="" class="" width="35em" height="35em" style="" ></span>
                <span style="font-size:20px;color:#dae5ec; ">Realizar Pedido </span>
             </strong>
         </div class="panel-heading clearfix">
        <fieldset>
        <form method="POST" id="Register" action="{{ route('pedido.store') }}">
                        {{ csrf_field() }}
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                        <!--================[]==================-->
                                        

                                        <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Cliente') }}</label>
                                        <select name="cliente" id="cliente" class="form-control" >
                                            
                                            @foreach ($users AS $user)
                                             <option value="{{ $user->name, $user->id }}"> {{ $user->name }}</option>

                                            
                                            @endforeach


                                        </select> 
                                    </div>

                                    </div>

                                    
                                    <input type="hidden" id="stockmin" value="{{ $stockmin->stockmin }}"></div>
                            
                            <!--================[]==================-->
                                        <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Producto') }}</label>
                                        <select name="producto" id="producto" class="form-control" >
                                            
                                            @foreach ($productos AS $producto)
                                             <option value="{{ $producto->id }}"> {{ $producto->nombre }}</option>

                                            
                                            @endforeach


                                        </select> 
                                    </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Precio') }}</label>
                                            
                                             <input id="preciopedido" type="number" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio') }}" required autocomplete="precio" autofocus>
                                             </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Cantidad') }}</label>
                                            
                                             <input id="cantidadpedido" type="number" class="form-control @error('cantidad') is-invalid @enderror" name="cantidad" value="{{ old('cantidad') }}" required autocomplete="cantidad" autofocus>

                                    </div>
                                        
                                    
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Total pedido') }}</label>
                                            
                                             <input id="totalpedido" type="number" class="form-control @error('totalpedido') is-invalid @enderror" name="totalpedido" value="{{ old('totalpedido') }}" required autocomplete="totalpedido" autofocus>
                                             </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('No. Pedido') }}</label>
                                            
                                             <input id="npedido" type="text" class="form-control @error('npedido') is-invalid @enderror" name="npedido" value="{{ old('npedido') }}" required autocomplete="npedido" autofocus>
                                             </div>

                                    </div>
                                    
                                    
                                    
                                <!--====================================-->
                            <!--=============[]===============-->
                            
                            <!--=============[]===============-->
                        </div>
                        <!--=======================================-->
                        <div class="form-group">
                            <!--=============[]===============-->
                        
                            <!--=============[]===============-->
                            
                            <!--=============[]===============-->
                            
                            
                        </div>
                        
                                <!--====================================-->
                            
                            <hr>
                                            <!--====================================-->
                                        <!--====================================-->
                           
                            <!--====================================-->
                            </div>
                        

                   
                </div>
            
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
            
        </form>
					
				</fieldset>
					</div>
    			</div>
						
			</div>

					@endsection        
