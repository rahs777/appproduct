
<div id="modalusercreate" class="modal fade" role="dialog">
    <div class="modal-dialog"   style="height:80%;width:80% ;">

    <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Ingresar Pedido</h3>
            </div>
            <form method="POST" id="Register" action="{{ route('pedido.store') }}">
                        {{ csrf_field() }}
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      
                    </div>
                </div>

                <div class="col-md-12">
                    
                       <div class="card1">

                                        
                                         

                                        <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Cliente') }}</label>
                                        <select name="cliente" id="cliente" class="form-control" >
                                            
                                            @foreach ($users AS $user)
                                             <option value="{{ $user->name, $user->id }}"> {{ $user->name }}</option>

                                            
                                            @endforeach


                                        </select> 
                                    </div>

                                    </div>

                                    
                                    <input type="hidden" id="stockmin" value="{{ $stockmin->stockmin }}"></div>
                            
                            <!--================[]==================-->
                                        <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Producto') }}</label>
                                        <select name="producto" id="producto" class="form-control" >
                                            
                                            @foreach ($productos AS $producto)
                                             <option value="{{ $producto->id }}"> {{ $producto->nombre }}</option>

                                            
                                            @endforeach


                                        </select> 
                                    </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Precio') }}</label>
                                            
                                             <input id="precio" type="number" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio') }}" required autocomplete="precio" autofocus>
                                             </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Cantidad') }}</label>
                                            
                                             <input id="cantidad" type="number" class="form-control @error('cantidad') is-invalid @enderror" name="cantidad" value="{{ old('cantidad') }}" required autocomplete="cantidad" autofocus>

                                    </div>
                                        
                                    
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('Total pedido') }}</label>
                                            
                                             <input id="totalcompra" type="text" class="form-control @error('totalcompra') is-invalid @enderror" name="totalcompra" value="{{ old('nfacttotalcompraura') }}" required autocomplete="totalcompra" autofocus>
                                             </div>

                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label"><i class="fa fa-briefcase"> </i> {{ __('No. Pedido') }}</label>
                                            
                                             <input id="totalpedido" type="text" class="form-control @error('totalpedido') is-invalid @enderror" name="totalpedido" value="{{ old('totalpedido') }}" required autocomplete="totalpedido" autofocus>
                                             </div>

                                    </div>
                                    
                                    
                                    
                                <!--====================================-->
                            <!--=============[]===============-->
                            
                            <!--=============[]===============-->
                        </div>
                        <!--=======================================-->
                        <div class="form-group">
                            <!--=============[]===============-->
                        
                            <!--=============[]===============-->
                            
                            <!--=============[]===============-->
                           
                            
                        </div>
                        

                   
                </div>
            </div>
            <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn"
                            value="create"> Registrar
                            </button>
                        </div>
        </form>
        </div>

    </div>
</div>
