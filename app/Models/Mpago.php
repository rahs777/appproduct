<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mpago extends Model
{
    protected $fillable = [
        'tipo','descripcion', ];
}
