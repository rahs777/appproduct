<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $fillable = [
        'cliente','producto', 'cantidad','precio','totalcompra','nfactura',
    ];
}
