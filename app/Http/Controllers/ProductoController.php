<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Producto;
use App\Models\Tabla;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $productos=Producto::orderBy('id','Desc')->paginate('5');
       
       $categorias=Categoria::orderBy('id')->get();
       
       $stockmin=Tabla::first();
       //$categorias=Categoria::find();
       //dd($tablas);
       return view('producto.index',compact('productos','categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias=Categoria::orderBy('id')->get();
      return view('producto.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([

          'nombre' => ['required', 'string', 'max:150'],
          'precio' => ['required', 'string', 'max:150'],
          'categoria' => ['required', 'string', 'max:150'],
          'stock' => ['required', 'string', 'max:12'],
          'descripcion' => ['required', 'string', 'max:150'],
          ]);
       //dd($request);
       $producto=Producto::create([
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'categoria' => $request->categoria,
            'stock' => $request->stock,
            'descripcion' => $request->descripcion
        ]);

        //$producto=Producto::create($request ->all());
         return redirect()->route('producto')
        ->with('status_success','Producto Guardado....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        $productos=Producto::orderBy('id')->get();
        //dd($producto);

    return view('producto.view', compact('producto'));
    }
    public function edit(Producto $producto)
    {
        $categorias=Categoria::orderBy('id')->get();
        return view('producto.edit', compact('producto','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
       $request->validate([

          'nombre' => ['required', 'string', 'max:150'],
          'precio' => ['required', 'string', 'max:150'],
          'categoria' => ['required', 'string', 'max:150'],
          'descripcion' => ['required', 'string', 'max:150'],
          ]);

        $producto->update([
            'nombre'      => $request['nombre'],
            'precio'     => $request['precio'],
            'stock'     => $request['stock'],
            'categoria'  => $request['categoria'],
            'descripcion'  => $request['descripcion']
            ]);

       //$producto=update($request ->all()); 
       return redirect()->route('producto')
        ->with('status_success','Producto Actualizado....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        
        return redirect()->route('producto')
         ->with('status_success','Producto Eliminado ....');
    }
}
