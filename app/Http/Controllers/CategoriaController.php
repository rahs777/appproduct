<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $categorias=Categoria::orderBy('id','Desc')->paginate('5');
       return view('categoria.index',compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

          'nombrecat' => ['required', 'string', 'max:150'],
          'descripcion' => ['required', 'string', 'max:150'],
         
         
          ]);
        $categoria=Categoria::create($request ->all());
         return redirect()->route('categoria')
        ->with('status_success','Categoria Registrada....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categorium)
    {
        $categorias=Categoria::orderBy('id')->get();
         return view('categoria.view', compact('categorium'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categorium)
    {
        $categorias=Categoria::orderBy('id')->get();
         return view('categoria.edit', compact('categorium'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categorium)
    {
       $request->validate([

          'nombrecat' => ['required', 'string', 'max:150'],
          'descripcion' => ['required', 'string', 'max:150'],
         
         
          ]);
       $categorium->update($request ->all());  
        return redirect()->route('categoria')
        ->with('status_success','Categoria Actualizada....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categorium)
    {
        $categorium->delete();
        
        return redirect()->route('categoria')
         ->with('status_success','Categoria Eliminada ....');
    }
}
