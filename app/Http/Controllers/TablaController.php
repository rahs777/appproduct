<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tabla;
class TablaController extends Controller
{
    
    public function index()
    {
        $tablas=Tabla::orderBy('id','Desc')->paginate('5');
       return view('tabla.index',compact('tablas'));
       
    }

   
    public function create()
    {
         return view('tabla.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([

          'stockmin' => ['required','max:10'],
          'iva' => ['required'],
         'montominimocompra' => ['required'],   
              
          ]);
        //dd($request);
        $tabla=Tabla::create($request ->all());
         return redirect()->route('tabla.index')
        ->with('status_success','tabla Registrada....');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit(Tabla $tabla)
    {
         
         return view('tabla.edit',compact('tabla'));
          
    }

  
    public function update(Request $request,Tabla $tabla)
    {
        $request->validate([

          'stockmin' => ['required'],
          'iva' => ['required'],
         'montominimocompra' => ['required'],   
              
          ]);
        //dd($request);
        $tabla->update([
            'stockmin'      => $request['stockmin'],
            'iva'     => $request['iva'],
            'montominimocompra'     => $request['montominimocompra']
            
            ]);
        //$tabla=Tabla::update($request ->all());
         return redirect()->route('tabla.index')
        ->with('status_success','tabla Registrada....');
    }

   
    public function destroy(Tabla $tabla)
    {
       $tabla->delete();
        
        return redirect()->route('tabla.index')
         ->with('status_success','tabla Succcefull Removed ....');
    }
}
