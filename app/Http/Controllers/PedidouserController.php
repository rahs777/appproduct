<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pedido;
use App\Models\Producto;
use App\Models\Mpago;
use App\Models\Tabla;
use App\User;
class PedidouserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        $tablas=Tabla::orderBy('id','Desc')->get();
        
        //$stockmin=Tabla::where('stockmin')->get();
        $stockmin=Tabla::first();
        $pedidos=Pedido::orderBy('id','Desc')->paginate('5');
       return view('cliente.pedido.index',compact('users','pedidos','mpagos','productos'))->with('stockmin', $stockmin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        $tablas=Tabla::orderBy('id','Desc')->get();
        $pedidos=Pedido::orderBy('id','Desc')->paginate('5');
       
         $stockmin=Tabla::first();
        //dd($stockmin);
       return view('cliente.pedido.create',compact('users','mpagos','productos'))->with('stockmin', $stockmin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'cliente' => ['required'],
          'producto' => ['required'],
          'cantidad' => ['required'],
          'precio' => ['required'],
          'totalpedido' => ['required']
          ]);
        /***********[ muestra el stop minimo configurado ]***************/
        $stockmin=Tabla::first();
       $stockmin=$stockmin->stockmin;
        if($request->cantidad >= $stockmin){

            return redirect()->route('clientepedido.index')
        ->with('status_error','No puede realizar la compra excede el stock minimo');
        }
       /************[ actualia el stock del producto vendido ]**************/
       $producto= Producto::find($request->producto);
       $cantvta=$request->cantidad;
       $stock=$producto->stock;
        $stockact=$stock-$cantvta;
       $producto->stock=$stockact;
        $producto->save();
        /**************************/
       $pedido=Pedido::create($request ->all());
        return redirect()->route('clientepedido.index')
        ->with('status_success','pedido Guardada....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pedido $pedido)
    {
        $pedido->delete();
        
        return redirect()->route('compra')
         ->with('status_success','Compra Eliminada ....');
    }
}
