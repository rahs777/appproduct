<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Compra;
use App\User;
use App\Models\Producto;
use App\Models\Mpago;
use App\Models\Tabla;
class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        $tablas=Tabla::orderBy('id','Desc')->get();
        $compras=Compra::orderBy('id','Desc')->paginate('5');
       
        $stockmin=Tabla::first();
        //dd($stockmin->stockmin);
       
    return view('compra.index')->with(compact('compras','users','productos','mpagos','tablas'),'stockmin', $stockmin);
       //return view('compra.index',compact('compras','users','productos','mpagos','tablas'))->with('stockmin', $stockmin);
       
    }
    public function compraproduct(Request $request,Producto $producto)
    {
        
        $producto = Producto::find($producto);
        $precio=$producto->precio;
               //$precio='1500';
        //dd($precio);
               return response()->json($precio);


    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        
        $stockmin=Tabla::first();
        //dd($stockmin->stockmin);
        return view('compra.create',compact('users','productos','mpagos'))->with('stockmin', $stockmin);
    }

   
    public function store(Request $request,Producto $producto)
    {
       $request->validate([
          'cliente' => ['required'],
          'producto' => ['required'],
          'cantidad' => ['required'],
          'precio' => ['required'],
          'nfactura' => ['required']
          ]);
       //dd($request);
       /***********[ muestra el stop minimo configurado ]***************/
        $stockmin=Tabla::first();
       $stockmin=$stockmin->stockmin;
        if($request->cantidad >= $stockmin){

            return redirect()->route('compra')
        ->with('status_error','No puede realizar la compra excede el stock minimo');
        }
       /************[ actualia el stock del producto vendido ]**************/
       $producto= Producto::find($request->producto);
       $cantvta=$request->cantidad;
       $stock=$producto->stock;
        $stockact=$stock-$cantvta;
       $producto->stock=$stockact;
        $producto->save();
        /**************************/
       $compra=Compra::create($request ->all());
        return redirect()->route('compra')
        ->with('status_success','Compra Guardada....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        
        $stockmin=Tabla::first();
        $compras=Compra::orderBy('id')->get();
        //dd($users);
        return view('compra.edit',compact('users','productos','mpagos','compra'))->with('stockmin', $stockmin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compra $compra)
    {

        $request->validate([
          'cliente' => ['required'],
          'producto' => ['required'],
          'cantidad' => ['required'],
          'precio' => ['required'],
          'nfactura' => ['required']
          ]);
       //dd($request);
        $compra->update([
            'cliente'      => $request['cliente'],
            'producto'     => $request['producto'],
            'cantidad'     => $request['cantidad'],
            'precio'       => $request['precio'],
            'totalcompra'  => $request['totalcompra'],
            'nfactura'     => $request['nfactura']
            ]);
       //$compra=Compra::update($request ->all());
        return redirect()->route('compra')
        ->with('status_success','Compra Actualiada....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compra $compra)
    {
       $compra->delete();
        
        return redirect()->route('compra')
         ->with('status_success','Compra Eliminada ....');
    }
}
