<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $users=User::orderBy('id','Desc')->paginate('5');
       return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255','unique:users'],
            'username' => ['required', 'string', 'max:255','unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        //dd($request);
         //$user=User::create($request ->all());
        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'status' => $request->status,
            'password' => Hash::make($request->password)
        ]);
        return redirect()->route('user.index')
        ->with('status_success','Usuario Guardado....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
         $request->validate([
            'name'  => 'required|max:50|unique:users,name,'.$user->id,
            'email'  => 'required|max:50|unique:users,email,'.$user->id,
            'password' => 'required|string|min:8|confirmed'
                        
        ]);
         //dd($request);
         $user->update([
            'name'      => $request['name'],
            'email'     => $request['email'],
            'username'  => $request['username'],
            'is_admin'  => $request['is_admin'],
            'status'    => $request['status'],
            'password'  => Hash::make($request['password'])
           
             ]);
         return redirect()->route('user.index')
        ->with('status_success','Usuario Actualizado....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        
        return redirect()->route('user.index')
         ->with('status_success','User Succcefull Removed ....');
    }
}
