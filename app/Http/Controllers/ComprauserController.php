<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Compra;
use App\User;
use App\Models\Producto;
use App\Models\Mpago;
use App\Models\Tabla;
use App\Http\Controllers\Auth;
class ComprauserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $compras=Compra::orderBy('id','Desc')->paginate('5');
        $nombprod=[];
        $cont=0;
        foreach ($compras as $compra) {
           //dd($compra->producto);
           $nombprod[] = Producto::find($compra->producto);
           //$nombprod[]=Producto::where('id','=',$compra->producto)->get();
           //dd($nombprod->nombre);
        }
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        $tablas=Tabla::orderBy('id','Desc')->get();
        $stockmin=Tabla::first();
       
    return view('cliente.compra.index',compact('compras','users','productos','mpagos','tablas','nombprod'))->with('stockmin', $stockmin);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $compras=Compra::orderBy('id','Desc')->paginate('5');
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        $tablas=Tabla::orderBy('id','Desc')->get();
         $stockmin=Tabla::first();
        return view('cliente.compra.create',compact('compras','users','productos','mpagos','tablas'))->with('stockmin', $stockmin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Producto $producto)
    {
       $request->validate([
          'cliente' => ['required'],
          'producto' => ['required'],
          'cantidad' => ['required'],
          'precio' => ['required'],
          'nfactura' => ['required']
          ]);
        /***********[ muestra el stop minimo configurado ]***************/
       $stockmin=Tabla::first();
       $stockmin=$stockmin->stockmin;
        if($request->cantidad >= $stockmin){

            return redirect()->route('clientecompra')
        ->with('status_error','No puede realizar la compra excede el stock minimo');
        }
      /************[ actualia el stock del producto vendido ]**************/
       $producto= Producto::find($request->producto);
       $cantvta=$request->cantidad;
       $stock=$producto->stock;
       $stockact=$stock-$cantvta;
       $producto->stock=$stockact;
       $producto->save();
 /**************************/
       $compra=Compra::create($request ->all());
        return redirect()->route('clientecompra')
        ->with('status_success','Compra Guardada....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
      $compras=Compra::orderBy('id','Desc')->paginate('5');
        $users=User::orderBy('id','Desc')->get();
        $productos=Producto::orderBy('id','Desc')->get();
        $mpagos=Mpago::orderBy('id','Desc')->get();
        $tablas=Tabla::orderBy('id','Desc')->get();$stockmin=Tabla::first();
        $compras=Compra::orderBy('id')->get();
        //dd($users);
        return view('cliente.compra.edit',compact('users','productos','mpagos','compra'))->with('stockmin', $stockmin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compra $compra)
    {
       $request->validate([
          'cliente' => ['required'],
          'producto' => ['required'],
          'cantidad' => ['required'],
          'precio' => ['required'],
          'nfactura' => ['required']
          ]);
       //dd($request);
        $compra->update([
            'cliente'      => $request['cliente'],
            'producto'     => $request['producto'],
            'cantidad'     => $request['cantidad'],
            'precio'       => $request['precio'],
            'totalcompra'  => $request['totalcompra'],
            'nfactura'     => $request['nfactura']
            ]);
       //$compra=Compra::update($request ->all());
        return redirect()->route('clientecompra')
        ->with('status_success','Compra Actualiada....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compra $compra)
    {
         $compra->delete();
        
        return redirect()->route('clientecompra')
         ->with('status_success','Compra Eliminada ....');
    }
}
