<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mpago;
class MpagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $mpagos=Mpago::orderBy('id','Desc')->paginate('5');
       return view('mpago.index',compact('mpagos'));
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    return view('mpago.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'tipo' => 'required|in :transfAch,contraentrega',
        'descripcion' => ['required', 'string', 'max:150'],
   
        ]);
 return redirect()->route('mpago')
        ->with('status_success','Metodo Pago Guardado....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mpago $mpago)
    {
      $mpagos=Categoria::orderBy('id')->get();
        return view('mpago.edit', compact('mpagos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'tipo' => 'required|in :transfAch,contraentrega',
          'descripcion' => ['required', 'string', 'max:150'],
        ]);
 return redirect()->route('mpago')
        ->with('status_success','metodo de pago Guardado....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
