<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    
    public function handle($request, Closure $next)
    {
        
        $test=auth()->user()->is_admin;
        //dd($test);
        //dd(auth()->user());
        if (! auth()->check())
            return redirect('login');

        if ($test != 1) //1 es admin 0 usuario
               return redirect('layouts/adminusuario');
             
       
        return $next($request);
       

    }
}
