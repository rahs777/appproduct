<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Policies\UserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    /**
     * Este una politica y se aplica o relaciona a un solo  modelo especifico.
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
    ];

    /********************************************************
     * Register any authentication / authorization services.
     *
     * @return void
     */
    /*******************************************************
     * Este es un gate se usa en todo el sistema.
     *
     * 
     *****************************************************/

    public function boot()
    {
        $this->registerPolicies();

        Gate::define( 'haveaccess', function (User $user, $perm){
            // dd($user->id);
           return $user->havePermission($perm);
         //return $perm;
              //return true;

        });

        //
    }
}
