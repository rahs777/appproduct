<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Auth\LoginController;

Route::get('/', function () {
    return view('auth/login');
});

/**/////////[                ]////////**/
/**/////////[ Grupo de route ]////////**/
/**/////////[                ]////////**/
/***[ rutas para los controlladores auth home ]**/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/*
Route::resource('/producto', 'ProductoController')->names('producto');
Route::resource('/categoria', 'CategoriaController');
Route::resource('/user', 'UserController');
Route::resource('/tabla', 'TablaController')->names('tabla');
*/
/***************************************************************/
Route::group(['middleware' => 'admin'], function () {
	/**/////////[  Routes  para Gestionar  vistas admin  ]////////**/
Route::get('layouts/admin360', 'Auth\LoginController@useradmin360')->name('layouts.admin360');
	/**/////////[  Routes  para Gestionar  Productos   ]////////**/
Route::get('/producto', 'ProductoController@index')->name('producto');
Route::get('/producto/create', 'ProductoController@create')->name('producto.create');
Route::post('/producto', 'ProductoController@store')->name('producto.store');
Route::get('/producto/{producto}', 'ProductoController@show')->name('producto.show');
Route::put('/producto/{producto}', 'ProductoController@update')->name('producto.update');
Route::delete('/producto/{producto}', 'ProductoController@destroy')->name('producto.destroy');
Route::get('/producto/{producto}/edit', 'ProductoController@edit')->name('producto.edit');
/**/////////[  Routes  para Gestionar  Categorias   ]////////**/
Route::get('/categoria', 'CategoriaController@index')->name('categoria');
Route::get('/categoria/create', 'CategoriaController@create')->name('categoria.create');
Route::post('/categoria', 'CategoriaController@store')->name('categoria.store');
Route::get('/categoria/{categorium}', 'CategoriaController@show')->name('categoria.show');
Route::put('/categoria/{categorium}', 'CategoriaController@update')->name('categoria.update');
Route::delete('/categoria/{categorium}', 'CategoriaController@destroy')->name('categoria.destroy');
Route::get('/categoria/{categorium}/edit', 'CategoriaController@edit')->name('categoria.edit');
/**/////////[  Routes  para Gestionar  user   ]////////**/
Route::get('/user', 'UserController@index')->name('user.index');
Route::get('/user/create', 'UserController@create')->name('user.create');
Route::post('/user', 'UserController@store')->name('user.store');
Route::get('/user/{user}', 'UserController@show')->name('user.show');
Route::put('/user/{user}', 'UserController@update')->name('user.update');
Route::delete('/user/{user}', 'UserController@destroy')->name('user.destroy');
Route::get('/user/{user}/edit', 'UserController@edit')->name('user.edit');

/**/////////[  Routes  para Gestionar  tabla   ]////////**/

Route::get('/tabla', 'TablaController@index')->name('tabla.index');
Route::get('/tabla/create', 'TablaController@create')->name('tabla.create');
Route::post('/tabla', 'TablaController@store')->name('tabla.store');
Route::get('/tabla/{tabla}', 'TablaController@show')->name('tabla.show');
Route::put('/tabla/{tabla}', 'TablaController@update')->name('tabla.update');
Route::delete('/tabla/{tabla}', 'TablaController@destroy')->name('tabla.destroy');
Route::get('/tabla/{tabla}/edit', 'TablaController@edit')->name('tabla.edit');

/**/////////[  Routes  para Gestionar  vistas admin  ]////////**/
Route::get('/mpago', 'MpagoController@index')->name('mpago');
Route::get('/mpago/create', 'MpagoController@create')->name('mpago.create');
Route::post('/mpago', 'MpagoController@store')->name('mpago.store');
Route::get('/mpago/{mpago}', 'MpagoController@show')->name('mpago.show');
Route::put('/mpago/{mpago}', 'MpagoController@update')->name('mpago.update');
Route::delete('/mpago/{mpago}', 'MpagoController@destroy')->name('mpago.destroy');
/**/////////[  Routes  para Gestionar  vistas Compra  ]////////**/
Route::get('/compra', 'CompraController@index')->name('compra');
Route::get('/compra/create', 'CompraController@create')->name('compra.create');
Route::post('/compra', 'CompraController@store')->name('compra.store');
Route::get('/compra/{compra}', 'CompraController@show')->name('compra.show');
Route::put('/compra/{compra}', 'CompraController@update')->name('compra.update');
Route::delete('/compra/{compra}', 'CompraController@destroy')->name('compra.destroy');
Route::get('/compra/{compra}/edit', 'CompraController@edit')->name('compra.edit');
Route::get('/compraproduct/{producto}', 'CompraController@compraproduct')->name('compra.ajax');;
/**/////////[  Routes  para Gestionar  vistas Pedido  ]////////**/
Route::get('/pedido', 'PedidoController@index')->name('pedido');
Route::get('/pedido/create', 'PedidoController@create')->name('pedido.create');
Route::post('/pedido', 'PedidoController@store')->name('pedido.store');
Route::get('/pedido/{pedido}', 'PedidoController@show')->name('pedido.show');
Route::put('/pedido/{pedido}', 'PedidoController@update')->name('pedido.update');
Route::delete('/pedido/{pedido}', 'PedidoController@destroy')->name('pedido.destroy');
Route::get('/pedido/{pedido}/edit', 'PedidoController@edit')->name('pedido.edit');

});

/**/////////[                                                  ]////////**/
/**/////////[  Routes  para Gestionar  vistas Para el Cliente  ]////////**/
/**/////////[                                                  ]////////**/


Route::group(['middleware' => 'auth'], function () {
Route::get('layouts/adminusuario', 'Auth\LoginController@useradmin')->name('layouts.adminusuario');

Route::get('layouts/adminusuario', function () {
    return view('layouts.adminusuario');
});
/**/////////[  Routes  para Gestionar  vistas Compra  ]////////**/
Route::get('/cliente/compra', 'ComprauserController@index')->name('clientecompra');
Route::get('/cliente/compra/create', 'ComprauserController@create')->name('clientecompra.create');
Route::post('/cliente/compra', 'ComprauserController@store')->name('clientecompra.store');
Route::get('/cliente/compra/{compra}', 'ComprauserController@show')->name('clientecompra.show');
Route::put('/cliente/compra/{compra}', 'ComprauserController@update')->name('clientecompra.update');
Route::delete('/cliente/compra/{compra}', 'ComprauserController@destroy')->name('clientecompra.destroy');
Route::get('/cliente/compra/{compra}/edit', 'ComprauserController@edit')->name('cliente.compra.edit');
/**/////////[  Routes  para Gestionar  vistas Pedido  ]////////**/
Route::get('/cliente/pedido', 'PedidouserController@index')->name('clientepedido.index');
Route::get('/cliente/pedido/create', 'PedidouserController@create')->name('clientepedido.create');
Route::post('/cliente/pedido', 'PedidouserController@store')->name('clientepedido.store');
Route::get('/cliente/pedido/{pedido}', 'PedidouserController@show')->name('clientepedido.show');
Route::put('/cliente/pedido/{pedido}', 'PedidouserController@update')->name('clientepedido.update');
Route::delete('/cliente/pedido/{pedido}', 'PedidouserController@destroy')->name('clientepedido.destroy');
Route::get('/cliente/pedido/{pedido}/edit', 'PedidouserController@edit')->name('clientepedido.edit');

});






