<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
        'name' =>'admin',
        'username' => 'admin',
        'email' => 'admin@admin',
        'is_admin'     => '1',
        'status' => '1',
        'password'=>Hash::make('11111111'),

       ]);
      User::create([
        'name' =>'usuario',
        'username' => 'usuario',
        'email' => 'usuario@usuario',
        'is_admin'     => '0',
        'status' => '0',
        'password'=>Hash::make('22222222'),

       ]);

      
    }
}
