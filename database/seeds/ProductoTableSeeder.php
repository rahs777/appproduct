<?php

use Illuminate\Database\Seeder;
use App\Models\Producto;
class ProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Producto::create([
       
       	'nombre' =>'Leche de lata',
		    'precio' => '123',
		    'categoria' => 'lacteos',
		    'stock'     => '5',
		    'descripcion' => 'ninguna'


       ]);

       Producto::create([
       	
        'nombre' =>'soporte',
        'precio' => '200',
        'categoria' => 'servicio',
        'stock'     => '5',
        'descripcion' => 'ninguna'


       ]);
    }

}
