<?php

use Illuminate\Database\Seeder;
use App\Models\Tabla;
class TablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tabla::create([
       	
       	'stockmin' =>'10',
		    'iva' => '12',
		    'montominimocompra' => '10',
		   
       ]);
    }
}
