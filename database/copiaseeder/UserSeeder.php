<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

       	
       	    'name' =>'admin',
		    'username' => 'admin',
		    'email' => 'admin@admin',
		    'is_admin'     => '1',
		    'status' => '1',
		    'password'=>Hash::make('11111111'),


       ]);
    }
}
